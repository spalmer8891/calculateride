import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class CalculateRide extends Application{

	public static void main(String[] args) {
	
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//create controls
		Label lbl_heading = new Label("Airport Ride");
		Label lbl_from = new Label("From:");
		Label lbl_extra = new Label("Extra");
		Label lbl_pets = new Label("Pets");
		Label lbl_use407 = new Label("Use 407");
		Label lbl_addTip = new Label("Add Tip?");
		Label lbl_totalFair = new Label("The total fair is:");
		lbl_totalFair.setVisible(false);
		Label lbl_totalFairValue = new Label();
		lbl_totalFairValue.setVisible(false);
		TextField txt_field = new TextField();
		CheckBox checkBox1 =  new CheckBox();
		CheckBox checkBox2 =  new CheckBox();
		CheckBox checkBox3 =  new CheckBox();
		CheckBox checkBox4 =  new CheckBox();
		Button btn_calculate = new Button("CALCULATE");

        GridPane gridPane = new GridPane();

        gridPane.add(lbl_heading, 0, 0, 3, 1); //row,column,colspan,rowspan
        gridPane.add(lbl_from, 1, 1, 1, 1);
        gridPane.add(txt_field, 2, 1, 1, 1);
        gridPane.add(checkBox1, 1, 3, 1, 1);
        gridPane.add(lbl_extra, 2, 3, 1, 1);
        gridPane.add(checkBox2, 1, 4, 1, 1);
        gridPane.add(lbl_pets, 2, 4, 1, 1);
        gridPane.add(checkBox3, 1, 5, 1, 1);
        gridPane.add(lbl_use407, 2, 5, 1, 1);
        gridPane.add(checkBox4, 1, 6, 1, 1);
        gridPane.add(lbl_addTip, 2, 6, 1, 1);
        
        gridPane.add(btn_calculate, 1, 7, 3, 1);
        gridPane.add(lbl_totalFair, 1, 8, 2, 1);
        gridPane.add(lbl_totalFairValue, 2, 8, 3, 1);
        
        GridPane.setMargin(lbl_heading, new Insets(0, 0, 10, 0));
        gridPane.setHalignment(lbl_heading, HPos.CENTER);
        GridPane.setMargin(btn_calculate, new Insets(10, 0, 0, 0)); //Insets(double top, double right, double bottom, double left)
        gridPane.setHalignment(btn_calculate, HPos.CENTER);
        GridPane.setMargin(lbl_totalFair, new Insets(10, 0, 0, 0));
        GridPane.setMargin(lbl_totalFairValue, new Insets(10, 0, 0, 40));
        
        
        gridPane.setPadding(new Insets(0,10,0,10));
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(5); //set column 1 width to 5%
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(20); //set column 2 width to 20%
        gridPane.getColumnConstraints().addAll(column1, column2);


        Scene scene = new Scene(gridPane, 250, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        btn_calculate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	
    	        	double extra_luggage = 0;
    	        	double pets = 0;
    	        	double use407 = 0;
    	        	double tip = 0;
    	        	double charges = 0;
    	        	double baseFare = 0;
    	        	double tax = 0;
    	        	double totalPrice = 0;
    	        	String from_location = txt_field.getText().toLowerCase();
    	        	
    	        	DecimalFormat df = new DecimalFormat("#.00");
    	        	
    	        	if(from_location.equals("cestar college"))
    	        	{
    	        		baseFare = 51 + 0.13;
    	        	}
    	        	else if(from_location.equals("brampton"))
    	        	{
    	        		baseFare = 38 + 0.13;
    	        	}
    	        	else {
    	        		 
    	        		lbl_totalFair.setVisible(true);
    		        	lbl_totalFairValue.setText("Please enter correct location");
    		        	lbl_totalFairValue.setVisible(true);
    		        	return;
    	        	}
    	        	
    	            charges += baseFare;
    	        	if(checkBox1.isSelected()) //extra luggage
    	        	{
    	        		extra_luggage = 10.00;
    	        		charges += extra_luggage;
    	        	}
    	        	
    	        	if(checkBox2.isSelected()) //pets
    	        	{
    	        		pets = 6.00;
    	        		charges += pets;
    	        	}
    	        	
    	        	if(checkBox3.isSelected()) //use 407
    	        	{
    	        		use407 = 0.25;
    	        		charges += use407;
    	        	}
    	        	
    	        	tax = charges * 0.13; //charges include base fare
    	        	
    	        	if(checkBox4.isSelected()) //tip
    	        	{     		
    	        		tip = (charges + tax) * 0.15;	
    	        	}
    	        	
    	        	totalPrice = charges + tax + tip;
    	        	
    	        	lbl_totalFair.setVisible(true);
    	        	lbl_totalFairValue.setText("$"+df.format(totalPrice));
    	        	lbl_totalFairValue.setVisible(true);
    	        	}//
        });
		
	}

}
